var _ = require('lodash');
var RSVP = require('rsvp');
var dynamo = require("dynamodb");
var Joi    = require('joi');
var AWS = require('aws-sdk');


var connectionPromise;

function DynamoDbAdapter(logger, opts) {
    var self = this;
    _.bindAll(self, 'get', 'set', 'del', 'isMemoryStore', 'getAllClientInfos', '_get');

    var cfg = {};



    if( opts.credentials && opts.credentials.file)
    {

        dynamo.AWS.config.loadFromPath(opts.credentials.file);
    }
    else if( opts.credentials)
    {
        dynamo.AWS.config.update({accessKeyId: opts.credentials, secretAccessKey: opts.credentials.access_key, region: opts.region.secret_access});
    }



    if( opts.region)
    {

        cfg.region= opts.region;
    }


    if( opts.endpoint)
    {
        cfg.endpoint= opts.endpoint;
    }

    if(opts.apiVersion)
    {
        cfg.apiVersion = opts.apiVersion;
    }


    dynamo.dynamoDriver(new AWS.DynamoDB(cfg));



    self.settings = {
        // Backwards compatible way of determining connectionUrl
        db: process.env["DYNAMO_DB_TABLE"] || opts.dbtable || "AddonSettings"
    };

    // Add logger
    if (opts.logging) {
        dynamo.log.level('info');
    }



    self.AddonSettings = dynamo.define(self.settings.db, {
        hashKey: 'clientKey',
        rangeKey: 'key',
        timestamps: false,

        schema: {
            clientKey: Joi.string(),
            key: Joi.string(),
            val: Joi.string()
        }
    });


    connectionPromise = new RSVP.Promise(function(resolve, reject)
    {

        self.AddonSettings.createTable(function (err)
        {
            if (err)
            {

                // More than likely the table already exists....
            } else
            {
                console.log('Tables has been created');
            }

            resolve(self.AddonSettings);

        });

    });


};

DynamoDbAdapter.prototype.isMemoryStore = function () {
    // Even if the mongoDB server was operating in in-memory mode, it’s not in the node process memory and won’t
    // disappear when the node service restarts, so from our perspective it’s still not an in-memory store
    return false;
};


// run a query with an arbitrary 'where' clause
// returns an array of values
DynamoDbAdapter.prototype._get = function (where)
{
    var self = this;

    return connectionPromise.then(function()
    {

        return new RSVP.Promise(function (resolve, reject)
        {

            return self.AddonSettings.scan()
                .where('key').gte(where.key)
                .attributes(['clientKey', 'key', 'val'])
                .returnConsumedCapacity()
                .exec(function (err,data)
                {

                    return resolve(data.Items.map(entry=>
                    {
                        return  JSON.parse(entry.attrs.val)
                    }));
                });


        });
    });

};

DynamoDbAdapter.prototype.getAllClientInfos = function () {
    var self = this;
    return self._get({key: 'clientInfo'});
};

// return a promise to a single object identified by 'key' in the data belonging to tenant 'clientKey'
DynamoDbAdapter.prototype.get = function (key, clientKey)
{
    var self = this;

    return connectionPromise.then(function()
    {

        return new RSVP.Promise(function (resolve, reject)
        {
            return self.AddonSettings.get( clientKey, key).then(function(entry)
                {

                   if(entry)
                    {
                       return resolve(getAsObject(entry.attrs.val));
                    }
                    else
                    {
                        return resolve(null);
                    }
                }).catch(reject);
        });

    });
};

DynamoDbAdapter.prototype.set = function (key, value, clientKey)
{
    var self = this;

    return connectionPromise.then(function()
    {

        return new RSVP.Promise(function (resolve, reject)
        {

            return self.AddonSettings.get( clientKey,  key).then(function(entry)
            {


                if (entry)
                {
                    return self.AddonSettings.update(
                        {
                            clientKey: clientKey,
                            key: key,
                            val: JSON.stringify(value)
                        }).then(function (b)
                    {
                        return resolve(getAsObject(value));
                    }).catch(reject);
                }
                else
                {
                   var acc = new self.AddonSettings({clientKey:clientKey, val: JSON.stringify(value), key: key});



                   return acc.save().then(function (entry)
                   {

                           return resolve( getAsObject(value));


                   }).catch(reject);

               }
            });


        });
    });

};

DynamoDbAdapter.prototype.del = function (key, clientKey) {
    var self = this;

    return connectionPromise.then(function()
    {

        return new RSVP.Promise(function (resolve, reject)
        {


            self.AddonSettings.destroy(clientKey , key, function ()
            {
                resolve();
            });
        });
    });
};



function getAsObject(val) {
    if (typeof val === "string") {
        try {
            val = JSON.parse(val);
        } catch (e) {
            // it's OK if we can't parse this. We'll just return the string below.
        }
    }

    return val;
}

function getObjectAsString(val) {
    if (typeof val === "string") {
        try {
            val = JSON.stringify(val);
        } catch (e) {
            // it's OK if we can't parse this. We'll just return the string below.
        }
    }

    return val;
}


module.exports = function (logger, opts) {
    if (0 === arguments.length) {
        return DynamoDbAdapter;
    }
    return new DynamoDbAdapter(logger, opts);
};
